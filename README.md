# Bypass Paywalls Clean filters

Adblocker list (experimental) which allows you to read articles from (supported) sites that implement a paywall.\
For some sites it will log you out (or block you to log in); caused by removing cookies or blocking general paywall-scripts.

Disclaimer: the list doesn't support as many sites as the extension/add-on though.

### Installation

Use a browser which supports extensions/add-ons and install an adblocker.

You can also install an app like AdGuard* (on Android & iOS) or AdLock (on iOS).\
So you can use it with Chrome/Firefox (on Android) or Safari (on iOS).

\* AdGuard Content Blocker (on Android) only works with Yandex Browser or Samsung Internet Browser when you add the filter (url) to user rules (manual update of filter required).\
Or use AdGuard app (from their site) which works for all apps (and automatically updates filter).

An external app may work less effective (timing issues).\
On iOS there's no support for scriptlets (for removing cookies, attributes and/or classes).

Now add custom (content)filter: [Bypass Paywalls Clean filters](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/bpc-paywall-filter.txt) or [subscribe](https://subscribe.adblockplus.org/?location=https%3A%2F%2Fgitlab.com%2Fmagnolia1234%2Fbypass-paywalls-clean-filters%2F-%2Fraw%2Fmain%2Fbpc-paywall-filter.txt&title=Bypass%20Paywalls%20Clean%20filters)

Also usefull list: [Fanboy's Enhanced Tracking List](https://www.fanboy.co.nz/enhancedstats.txt) or [subscribe](https://subscribe.adblockplus.org/?location=https%3A%2F%2Fwww.fanboy.co.nz%2Fenhancedstats.txt&title=Fanboy%27s%20Enhanced%20Tracking%20List)